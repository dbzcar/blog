<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Photo extends Model
{
    protected  $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($photo){
            $photoPath = str_replace('storage', 'public', $photo->url);//reemplaza palabra storage por public

            Storage::delete($photoPath);
        });
    }
}
