<?php
use App\Tag;
use App\Post;
use App\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Storage::disk('public')->deleteDirectory('posts');

        Post::truncate();
        Category::truncate();
        Tag::truncate();

        $category = new Category;
        $category->name= "Categoria 1";
        $category->save();

        $category = new Category;
        $category->name= "Categoria 2";
        $category->save();

        $post = new Post;
        $post->title = "Mi Primer Post";
        $post->url = str_slug("Mi Primer Post");
        $post->excerpt= "Extracto de mi Primer Post";
        $post->body="<p>Contenido de mi primer post</p>";
        $post->published_at = Carbon::now()->subDays(4);
        $post->category_id=1;
        $post->user_id=1;
        $post->save();

        $post->tags()->attach(Tag::create(['name' => 'etiqueta 1']));

        $post = new Post;
        $post->title = "Mi Segundo Post";
        $post->url = str_slug("Mi Segundo Post");
        $post->excerpt= "Extracto de mi Segundo Post";
        $post->body="<p>Contenido de mi Segundo post</p>";
        $post->published_at = Carbon::now()->subDays(3);
        $post->category_id=1;
        $post->user_id=1;
        $post->save();

        $post->tags()->attach(Tag::create(['name' => 'etiqueta 2']));

        $post = new Post;
        $post->title = "Mi Tercer Post";
        $post->url = str_slug("Mi Tercer Post");
        $post->excerpt= "Extracto de mi Tercer Post";
        $post->body="<p>Contenido de mi Tercer post</p>";
        $post->published_at = Carbon::now()->subDays(2);
        $post->category_id=2;
        $post->user_id=2;
        $post->save();

        $post->tags()->attach(Tag::create(['name' => 'etiqueta 3']));

        $post = new Post;
        $post->title = "Mi Cuarto Post";
        $post->url = str_slug("Mi Cuarto Post");
        $post->excerpt= "Extracto de mi Cuarto Post";
        $post->body="<p>Contenido de mi Cuarto post</p>";
        $post->published_at = Carbon::now()->subDays(1);
        $post->category_id=2;
        $post->user_id=2;
        $post->save();

        $post->tags()->attach(Tag::create(['name' => 'etiqueta 4s']));

    }
}
