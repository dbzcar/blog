@extends('admin.layout')

@section('header')
<section class="content-header">
    <h1>
      Todas las publicaciones
      <small>Todos los Posts</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Posts</li>
    </ol>
  </section>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Listado de Publicaciones</h3>

      <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">
        <i class="fa fa-plus"></i>Crear publicacion</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="posts-table" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>ID</th>
              <th>Titulo</th>
              <th>Extracto</th>
              <th>Acciones</th>
            </tr>
            </thead>

            <tbody>
                    @foreach($posts as $post)
      
                    <tr>
                        <td>{{$post->id}}</td>
                        <td>{{$post->title}}</td>
                        <td>{{$post->excerpt}}</td>
                        <td>
                        <a href="{{ route('posts.show', $post) }}"
                            class="btn btn-default btn-sm"
                            target="_blank"
                         ><i class="fa fa-eye"></i></a>

                        <a href="{{ route('admin.posts.edit', $post) }}"
                            class="btn btn-info btn-sm"
                            ><i class="fa fa-pencil"></i></a>

                        <form method="POST" 
                        action="{{ route('admin.posts.destroy', $post) }}"
                        style="display: inline">
                        {{ csrf_field() }} {{ method_field('DELETE') }}

                        <button class="btn btn-danger btn-sm"
                          onclick="return confirm('Estas seguro de querer eliminar esta publicacion?')"
                        ><i class="fa fa-times"></i></button>

                        </form>

                            <!--<a href="" class="btn btn-success btn-sm"><i class="fa fa-times"></i></a> -->
                        </td>
                    </tr>
      
                    @endforeach
      
            </tbody>
            
          </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

@stop

@push('styles')
 <!-- DataTables -->
 <link rel="stylesheet" href="/adminlte/plugins/datatables/dataTables.bootstrap.css">
@endpush

@push('scripts')
<!-- DataTables -->
<script src="/adminlte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/adminlte/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
    $(function () {
      $('#posts-table').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>

  @endpush
