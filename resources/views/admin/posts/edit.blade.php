@extends('admin.layout')

@section('header')
    <section class="content-header">
        <h1>
          POSTS
          <small>Crear Publicacion</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
          <li><a href="{{route('admin.posts.index')}}"><i class="fa fa-list"></i> Posts</a></li>
          <li class="active">Crear</li>
        </ol>
    </section>
@stop

@section('content')

<div class="row">
    @if ($post->photos->count())
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
            
                    @foreach($post->photos as $photo)
                        <form  method="POST" action=" {{ route('admin.photos.destroy', $photo) }}">
                            {{ method_field('DELETE') }} {{ csrf_field() }}   
                                <div class="col-md-2">
                                                         
                                    <button class="btn-danger" style="position: absolute"><i class="fa fa-remove"></i></button>
                                    <img src="{{ url($photo->url) }}" class="img-responsive">
                              
                                </div>
    
                        </form>
                     
                    @endforeach
            
                </div>
            </div>
        </div>

    @endif


    <form method="POST" action="{{route('admin.posts.update', $post)}}">
        {{csrf_field()}} {{ method_field('PUT') }}
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group {{ $errors->has('title') ? 'has-error': '' }}">
                            <label>Titulo de la Publicacion</label>
                                    <input class="form-control" placeholder="Ingresa Titulo" name="title"
                                    value="{{old('title', $post->title)}}">
                                    
                                    {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
                                    
                        </div>
                                      
                        <div class="form-group {{ $errors->has('body') ? 'has-error': '' }}">
                            <label>Contenido de la Publicacion</label>
                            <textarea id="editor" name="body" class="form-control" placeholder="Ingresa contenido completo de la publicacion" rows="10">{{old('body', $post->body)}}</textarea>
                            {!! $errors->first('body', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group {{ $errors->has('iframe') ? 'has-error': '' }}">
                            <label>Contenido embebido (iframe)</label>
                            <textarea id="editor" name="iframe" class="form-control" placeholder="Ingresa contenido embebido (iframe) de audio o video" rows="2">{{old('iframe', $post->iframe)}}</textarea>
                            {!! $errors->first('iframe', '<span class="help-block">:message</span>') !!}
                        </div>

                    </div>

                </div>                
            </div>

        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body">

                    <div class="form-group">
                        <label>Fecha de Publicacion:</label>
                            
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                                <input name="published_at" type="text" class="form-control pull-right" id="datepicker" value="{{old('published_at', $post->published_at ? $post->published_at ->format('m/d/Y') : null) }}">
                            </div>
                                    <!-- /.input group -->
                    </div>
                                          
                    <div class="form-group {{ $errors->has('category_id') ? 'has-error': '' }}">
                        <label>Categorias</label>
                            <select class="form-control select2" name="category_id">
                                <option value="">Selecciona una categoria</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"
                                        {{old('category_id', $post->category_id ) == $category->id ? 'selected': ''}}
                                        >{{ $category->name }}</option>

                                    @endforeach
                            </select>

                            {!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('category') ? 'has-error': '' }}">
                        <label>Etiquetas</label>
                            <select class="form-control select2"
                                    multiple="multiple" 
                                    data-placeholder="Selecciona una o mas etiquetas" 
                                    style="width: 100%;" name="tags[]">

                                        @foreach($tags as $tag)
                                          <option {{collect(old('tags', $post->tags->pluck('id') ))->contains($tag->id)? 'selected' : '' }} value="{{$tag->id}}">{{$tag->name}}</option>

                                        @endforeach
                            </select>

                            {!! $errors->first('tags', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {{ $errors->has('excerpt') ? 'has-error': '' }}">
                        <label>Extracto</label>
                            <textarea name="excerpt" class="form-control" placeholder="Ingresa un extracto de la publicacion">{{ old('excerpt', $post->excerpt)}}</textarea>
                                {!! $errors->first('excerpt', '<span class="help-block">:message</span>') !!}
                    </div>
        
                    <div class="form-group">
                        <div class="dropzone"></div>
                    </div>
        
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Guardar Publicacion</button>
                    </div>

                </div>

            </div>
        </div>

    </form>

</div>               


@stop


@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="/adminlte/plugins/datepicker/datepicker3.css">

<!-- Select2 -->
<link rel="stylesheet" href="/adminlte/plugins/select2/select2.min.css">

@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.js"></script>

<!-- bootstrap datepicker -->
<script src="/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- CK Editor -->
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>

<!-- Select2 -->
<script src="/adminlte/plugins/select2/select2.full.min.js"></script>

<script>
//Date picker
$('#datepicker').datepicker({
  autoclose: true
});
$('.select2').select2({
    tags: true
});

CKEDITOR.replace('editor');
CKEDITOR.config.height = 316;

var myDropzone = new Dropzone('.dropzone',{
    url:'/admin/posts/{{ $post->url }}/photos',
    paramName: 'photo',
    acceptedFiles: 'image/*',
    maxFilesize: 2,
    //maxFiles: 10,
    headers:{
        'X-CSRF-TOKEN': '{{ csrf_token() }}'
    },
    dictDefaultMessage: 'Arrastra las fotos aqui para subirlas'
});

myDropzone.on('error', function(file, res){
    var msg = res.errors.photo[0];
    $('.dz-error-message:last > span').text(msg);
});

Dropzone.autoDiscover = false;

</script>

@endpush